import axios from 'axios';

const api = axios.create({
    baseURL: 'http://192.168.1.20:3000/api'
})

//create api calls to the server end points 
//axios -> <- express router
export const insertRecipe = (payload) => api.post(`/recipe`, payload);
export const getRecipeById = (id) => api.get(`/recipe/${id}`);
export const getAllRecipes = () => api.get(`/recipes`);
export const updateRecipeById = (id, payload) => api.put(`/recipe/${id}`, payload);
export const deleteRecipeById = (id) => api.delete(`/recipe/${id}`);

const apis = {
    insertRecipe,
    getRecipeById,
    getAllRecipes,
    updateRecipeById,
    deleteRecipeById,
}

export default apis
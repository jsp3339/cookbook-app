import React, {Component} from 'react';
import './Logo.css';
//import styled from 'styled-components'; 

import logo from '../logo.svg';

class Logo extends Component {
    render() {
        return (
            <div>
                <img src={logo} className='logo-img' alt="logo"/>
            </div>
        )
    }
}

export default Logo
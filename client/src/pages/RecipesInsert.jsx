import React, {Component} from 'react'
import api  from '../api'
import './RecipesInsert.css'

const RecipesInsert = (props) => {
    let [name, setName] = React.useState("");
    let [time, setTime] = React.useState("");
    let [ingredients, setIngredients] = React.useState("");

    let handleChangeName = (event) => {
        setName(event.target.value);
    }

    let handleChangeTime = (event) => {
        setTime(event.target.value);
    }

    let handleChangeIngredients = (event) => {
        setIngredients(event.target.value); 
    }

    //on submit send payload as api call to create new recipe
    //using async/await = await makes JS runtime pause code on that line and won't continue execting code until async function is returned
    /*let onSubmit = async () => { 
        let ingred = ingredients.split('/');

        let payload = {
            "name": name,
            "time": time,
            "ingredients": ingred,
        };

        console.log(payload);
        await api.insertRecipe(payload).then( res => {
            console.log("Recipe Sent Successfully!");
            setName("");
            setTime("");
            setIngredients("");
        })

        console.log("SUBMIT");
    }*/

    let onSubmit = () => { 
        let ingred = ingredients.split('/');

        let payload = {
            "name": name,
            "time": time,
            "ingredients": ingred,
        };

        console.log(payload);
        api.insertRecipe(payload).then( res => {
            console.log("Recipe Sent Successfully!");
            setName("");
            setTime("");
            setIngredients("");
        })

        console.log("SUBMIT");
    }

    let onCancel = () => {
        window.location.href = "/recipes/list"; 
        console.log("CANCEL");
    }

    return (
        <div className="wrapper">
            <p className="fieldName">Name:</p>
            <input type="text" onChange={handleChangeName}></input>

            <p className="fieldName">Time:</p>
            <input type="text" onChange={handleChangeTime}></input>

            <p className="fieldName">Ingredients:</p>
            <input type="text" onChange={handleChangeIngredients}></input>

            <div className="buttonsWrapper">
                <button className="buttonStyle" onClick={onSubmit}>Add Recipe!</button>
                <button className="buttonStyle" onClick={onCancel}>Cancel</button>
            </div>
        </div>
    )
}

/*
const RecipesInsert = (props) => { //functional component
    let [name, setName] = React.useState('');
    let [time, setTime] = React.useState('');
    let [ingredients, setIngredients] = React.useState('');

    return (
        <div>
            <p>Creating a Recipe!</p>
        </div>
    )
}
*/

/*Class Component*/ /* //OLD CODE - JUST USING AS A CLASS COMPONENT EXAMPLE
class RecipesInsert extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            time: '',
            ingredients: '',
        }
    }

    handleChangeInputName = async event => {
        const name = event.target.value
        this.setState({ name })
    }

    handleChangeInputTime = async event => {
        const time = event.target.validity.valid
            ? event.target.value
            : this.state.time

        this.setState({ time })
    }

    handleChangeInputIngredients = async event => {
        const ingredients = event.target.value
        this.setState({ ingredients })
    }

    handleIncludeRecipe = async () => {
        const { name, time, ingredients } = this.state
        const arrayIngredients = ingredients.split('/')
        const payload = { name, time, ingredients: arrayIngredients }

        await api.insertRecipe(payload).then(res => {
            window.alert(`Recipe inserted successfully`)
            this.setState({
                name: '',
                time: '',
                ingredients: '',
            })
        })
    }

    render() {
        const { name, time, ingredients } = this.state
        return (
            <Wrapper>
                <Title>Create Recipes!!!</Title>

                <Label>Name: </Label>
                <InputText
                    type="text"
                    value={name}
                    onChange={this.handleChangeInputName}
                />

                <Label>Time: </Label>
                <InputText
                    type="number"
                    step="0.1"
                    lang="en-US"
                    min="0"
                    max="10"
                    pattern="[0-9]+([,\.][0-9]+)?"
                    value={time}
                    onChange={this.handleChangeInputTime}
                />

                <Label>Ingredients: </Label>
                <InputText
                    type="text"
                    value={ingredients}
                    onChange={this.handleChangeInputIngredients}
                />

                <Button onClick={this.handleIncludeRecipe}>Add Recipe</Button>
                <CancelButton href={'/recipes/list'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}*/

export default RecipesInsert
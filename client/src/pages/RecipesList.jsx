import React, {Component} from 'react'
import ReactTable from 'react-table'
import api from '../api'
import "./RecipesList.css"

import styled from 'styled-components'
//import 'react-table/react-table.css'

const Wrapper = styled.div`
    padding: 0 40px 40px 40px;
`

class RecipesList extends Component {
    constructor(props){
        super(props)
        this.state = {
            recipes: [],
            columns: [],
            isLoading: false,
        }
    }

    componentDidMount = async () => {
        this.setState({isLoading: true});

        await api.getAllRecipes().then(recipes => {
            this.setState({
                recipes: recipes.data.data,
                isLoading: false,
            });
        })

    }
    render() {
        const {recipes, isLoading} = this.state;
        console.log('RecipesList -> render -> recipes', recipes);
        console.log(recipes.length);

        /*
        const columns = [
            {
                Header: 'ID',
                accessor: '_id',
                filterable: true,
            },
            {
                Header: 'Name',
                accessor: 'name',
                filterable: true,
            },
            {
                Header: 'Time',
                accessor: 'time',
                filterable: true,
            },
            {
                Header: 'Ingredients',
                accessor: 'ingredients',
                Cell: props => <span>{props.value.join(' / ')}</span>,
            },
        ]*/

        let showTable = true;
        if (!recipes.length) {
            showTable = false;
        }

        return (
            //<React.Fragment>
               // {showTable && (
                    <table className="recipesTable">
                    <tbody>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Time</th>
                            <th>Indgredients</th>
                        </tr>
                        {recipes.map((recipe, i) => (
                            <tr key={i}>
                                <td>{recipe._id}</td>
                                <td>{recipe.name}</td>
                                <td>{recipe.time}</td>
                                <td>{recipe.ingredients.join('/')}</td>
                            </tr>
                        ))}
                    </tbody>
                    </table>
                    // <ReactTable
                    //     data={recipes}
                    //     columns={columns}
                    //     loading={isLoading}
                    //     defaultPageSize={10}
                    //     showPageSizeOptions={true}
                    //     minRows={0}
                    // />
           //     )}
           // </React.Fragment>
        )
    }
}

export default RecipesList
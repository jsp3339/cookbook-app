//contains all the CRUD operations for a recipe
//access the model for a recipe
const Recipe = require('../models/recipe-model');

//Create
createRecipe = (req, res) => {
    const body = req.body; //get request body (data)

    if(!body) { //make sure data is sent in
        return res.status(400).json({
            success: 'false',
            error: 'No recipe provided',
        })
    }

    const recipe = new Recipe(body); //create a recipe obj with the body data

    if(!recipe) { //make sure recipe obj is valid
        return res.status(400).json({success: false, error: 'Recipe not valid'});
    }

    recipe //save the recipe to the database and return successful api call
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: recipe._id,
                message: 'Recipe Created!!!',
            })
        }).catch(error => {
            return res.status(400).json({
                error,
                message: 'Recipe not created',
            })
        })
}

//Read
getRecipeById = async (req, res) => {
    await Recipe.findOne({ _id: req.params.id }, (err, recipe) => { //use await to ensure async data was received before continuing 
        if(err){
            return res.status(400).json({success: false, error: err});
        }

        if(!recipe){
            return res.status(404).json({success: false, error: 'Recipe not found'})
        }

        return res.status(200).json({success: true, data: recipe})
    }).catch(err => console.log(err))
}

getRecipes = async (req, res) => {
    await Recipe.find({}, (err, recipes) => {
        if(err){
            return res.status(400).json({success: false, error: err});
        }

        if(!recipes.length){
            return res.status(404).json({success: false, error: 'Recipes not found'})
        }

        return res.status(200).json({success: true, data: recipes})
    }).catch(err => console.log(err))
}

//Update
updateRecipe = async (req, res) => {
    const body = req.body; 

    if(!body) { //make sure data is sent in
        return res.status(400).json({
            success: 'false',
            error: 'No recipe provided',
        })
    }

    Recipe.findOne({ _id: req.params.id }, (err, recipe) => {
        if(err){
            return res.status(400).json({success: false, error: 'Recipe not found'});
        }

        recipe.name = body.name;
        recipe.time = body.time;
        recipe.ingredients = body.ingredients;

        recipe
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: recipe._id,
                    message: 'Recipe updated!!!',
                })
            }).catch(err => {
                return res.status(404).json({
                    err,
                    message: 'Recipe not updated',
                })
            })
    })
}

//Delete
deleteRecipe = async (req, res) => {
    await Recipe.findOneAndDelete({ _id: req.params.id }, (err, recipe) => {
        if(err){
            return res.status(400).json({
                success: false,
                error: err,
            })
        }

        if(!recipe){
            return res.status(404).json({
                success: false,
                error: 'Recipe not found',
            })
        }

        return res.status(200).json({
            success: true,
            data: recipe,
        })
    }).catch(err => console.log(err));
}

module.exports = {
    createRecipe,
    getRecipeById,
    getRecipes,
    updateRecipe,
    deleteRecipe,
}
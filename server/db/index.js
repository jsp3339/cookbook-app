const mongoose = require("mongoose");

mongoose
    //use "mongo" instead of 127.0.0.1 because in docker-compose different services are discoverable at a host name identical to the container name
    .connect('mongodb://mongo:27017/food', { useNewUrlParser: true }) 
    .catch(e => {
        console.error('Connection error', e.message);
    })

const db = mongoose.connection;

module.exports = db;
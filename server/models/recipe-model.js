const mongoose = require('mongoose');
const Schema = mongoose.Schema

const Recipe = new Schema(
    {
        name: {type: String, required: true},
        time: {type: Number, required: true},
        ingredients: {type: [String], required: true}
    },
    {timestamp:true},
)

module.exports = mongoose.model('recipes', Recipe);
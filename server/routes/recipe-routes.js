const express = require('express');
const RecipeCtrl = require('../controllers/recipe-ctrl');
const router = express.Router();

router.post('/recipe', RecipeCtrl.createRecipe);
router.get('/recipe/:id', RecipeCtrl.getRecipeById);
router.get('/recipes', RecipeCtrl.getRecipes);
router.put('/recipe/:id', RecipeCtrl.updateRecipe);
router.delete('/recipe/:id', RecipeCtrl.deleteRecipe);

module.exports = router; 